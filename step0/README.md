**Author**: Jacques Saraydaryan, All rights reserved
# Step 0: Création d'un projet Maven pour Springboot

## 1 Contexte
Springboot est un framework qui permet la création rapide d'applications notamment Web. Springboot utilise le principe "Convention over Configuration", c'est à dire que tout est pré-configuré avec une notation qui suit des conventions permettant la mise en oeuvre rapide d'applications. SpringBoot ne suit pas exactement les spécifications JEE, mais offre une mise en oeuvre beaucoup plus rapide et légère. Springboot offre aussi la possibilité d'intégrer un server http à l'archive générée permettant à notre application d'être totalement autonome.

# Création d'un projet via l'utilitaire Spring `start.spring.io` 
## 2.2 Génération du projet
Springboot propose également un générateur de projet permettant de sélectionner les dépendances nécessaires et générant un projet (.zip) maven.
La norme reste d'aller générer son projet (ou son fichier maven pom.xml) sur ce site `https://start.spring.io/`.


- Configurer un projet comme suit de :
  - type `Maven`  
    > définit le type de gestionnaire de projet utilisé. Ici Maven va gérer le cycle de vie de l'application ainsi que les dépendances associées
  - Langage `Java`
    > spécifie le langage utilisé. Spring support Java, Kotlin et Groovy
  - Springboot `3.4.2`
    > Indique la version de Spring Boot utilisé (préférer les lts..)
  - Meta Data:
    > Information de description de projet nécessaire à Maven
    - Group: `com.tuto.springboot`
    - Artifact: `SPWebApp`
    - Name: `SPWebApp`
    - Description: `Tuto project for Spring Boot`
    - Package Name: `com.sp`
    - Packaging: `jar`
    - Java: `21`
  - Dependencies
    > Listes des dépendanes et élements préconfigurés (starter) à ajouter à notre projet
    - Spring Web
    - Spring Data JPA
    - H2 Database

<img alt="img New Maven Project" src="./images/Spring_Initializr.png" width="800">

### 2.3 Application vide Springboot
- Cliquer sur le bouton `Generate`. Un zip d'un template de projet est alors téléchargé
- Dans un dossier qui vous servira de dossier de développement dézipper le template de projet.
- Ouvrer votre IDE (Intellij, Eclipse) et importer le projet maven.
- Dans ```src/main/java```, puis dans le package ```com.sp``` renommer le fichier et la classe `SpWebAppApplication.java` par `SpAppHero.java` 
- Le fichier alors modifié contient les éléments suivants:
```java
package com.sp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpAppHero {
	
	public static void main(String[] args) {
		SpringApplication.run(SpAppHero.class,args);
	}
}
```
- Explications
    ```java 
    ...
    @SpringBootApplication
    ...
    ```
    - Permet l'activation  (détail ici https://docs.spring.io/spring-boot/reference/using/using-the-springbootapplication-annotation.html):
        - de la configuration automatique de Springboot
        - du scanne des fichiers du package courant pour trouver des annotations spécifiques (e.g @component, @service,...)
        - de l'import de configurations additionnelles au besoin
    ```java 
    ...
   public static void main(String[] args) {
		SpringApplication.run(SpAppHero.class,args);
	}
    ...
    ```
    - Permet de lancer l'application Springboot (https://docs.spring.io/spring-boot/docs/current/api/org/springframework/boot/SpringApplication.html)

- Packager votre projet à l'aide de Maven 
  - directement en ligne de commande à la racine de votre projet `mvn package`
  - via votre IDE
    - Intellij: `Onglet Maven --> <votre projet> --> Lifecycle --> package`
    - Eclipse:  click droit sur votre projet, `Run As --> Maven Install`
- Lancer votre application Springboot 
  - Intellij: clic droit sur le fichier `SpAppHero.java` -> `Run SpAppHero.main()`
  - Eclipse: clic droit sur le fichier `SpAppHero.java` -> `Run As` -> `Java Application`
- Votre application Springboot démarre. Le résultat suivant devrait apparaître dans la console de votre application:
```


  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::                (v3.2.2)

2024-01-29T14:15:41.751+01:00  INFO 18376 --- [           main] com.sp.SpAppHero                         : Starting SpAppHero using Java 17.0.8.1 with PID 18376 (D:\project\ASI\ASI-1\ws\asi1-springboot-tuto\step0\target\classes started by jacques.saraydaryan in D:\project\ASI\ASI-1\ws\asi1-springboot-tuto)
2024-01-29T14:15:41.754+01:00  INFO 18376 --- [           main] com.sp.SpAppHero                         : No active profile set, falling back to 1 default profile: "default"
...
...
2024-01-29T14:15:44.272+01:00  INFO 18376 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port 8080 (http) with context path ''
2024-01-29T14:15:44.280+01:00  INFO 18376 --- [           main] com.sp.SpAppHero                         : Started SpAppHero in 2.877 seconds (process running for 3.21)

```


### 2.4 Contenu du projet
- A la racine de votre projet, vous trouverez le `pom.xml`, descripteur du projet Maven, qui contient les informations suivantes:

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

	<modelVersion>4.0.0</modelVersion>
	<groupId>com.tuto.springboot</groupId>
	<artifactId>SPWebApp</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<packaging>jar</packaging>
	
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>3.4.2</version>
		<relativePath /> <!-- lookup parent from repository -->
	</parent>
	
	<name>SPWebApp</name>
	<description>Project Sample for using Springboot</description>

	<properties>
		<java.version>21</java.version>
	</properties>

	<dependencies>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		<dependency>
			<groupId>com.h2database</groupId>
			<artifactId>h2</artifactId>
			<scope>runtime</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-tomcat</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
		</plugins>
	</build>

</project>
```
- Explications:
    ```xml
    ...
    <parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>3.4.2</version>
		<relativePath /> <!-- lookup parent from repository -->
	</parent>
    ...
    ```
    - Notre projet va hériter des propriétés d'un autre projet. Nous allons construire ainsi notre projet Springboot à partir d'un modèle d'application. La balise ```<parent>``` permet d'indiquer un projet Maven parent duquel nous allons hériter toutes les propriétés et dépendances.

    ```xml
    ...	
    <properties>
        <java.version>21</java.version>
    </properties>
    ...
    ```
    - La balise ```<properties>``` permet d'indiquer des propriétés supplémentaires à notre projet. Ici nous souhaitons que notre application soit compilée à l'aide de java 21

    ```xml
    ...	
	<dependencies>

        <dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
        ...
		
	</dependencies>
        ...
    ```
    - Les balises ```<dependencies>```  et ```<dependency>``` permettent de définir les librairies à ajouter à notre projet en dépendances. Toutes ces librairies sont des projets Maven qui vont être téléchargées en local sur notre machine depuis des repo. extérieurs (e.g Maven Repository https://mvnrepository.com/repos/central). Ces libraires (e.g *.jar) sont stockées en général dans un répertoire ```.m2``` à la racine du dossier personnel de l'utilisateur.
    - Il est possible de préciser les versions des libraires que l'on souhaite utiliser en utilisant la balise ```<version>```à l'intérieur de ```<dependency>```.
    - Ici on ajoute l'ensemble des dépendances que nous allons utiliser par la suite:
        - ```spring-boot-starter-web```: Ensemble d'outils pour la création d'applications Web (Restful, Spring MVC,...)
        - ```spring-boot-starter-data-jpa``` : Utilitaire permettant de mapper les classes java en éléments d'une base de données et de gérer la persistance avec la base de données (Spring Data JPA with Hibernate)
        - ```h2``` : Connecteur vers une base de données (ici H2 est une base de données embarquées https://www.h2database.com/html/main.html)
        - ```spring-boot-starter-tomcat``` : Serveur tomcat embarqué qui chargera notre application Web.
        - ```spring-boot-starter-test``` : Ensemble d'utilitaires pour tester notre application Web (e.g JUnit, Hamcrest and Mockito )
    ```xml
    ...	
    <build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
		</plugins>
	</build>
    ...	
    ```
    - La balise ```build``` contient l'ensemble des éléments permettant de gérer le cycle de vie de notre application.
    - Ici le plugin ```spring-boot-maven-plugin``` va permettre de compiler et packager notre application suivant les règles indiquées dans ce plugin (https://docs.spring.io/spring-boot/docs/current/maven-plugin/usage.html)


- Vous retrouverez la strucutre de projet suivante:


<img alt="img New Maven Project" src="./images/ProjectTree.png" width="200">

  - Explications:
    - `src/main` : contient toutes les sources nécessaires pour compiler votre application:
      - `src/main/java`: votre code source java
      - `src/main/test`: vos classe de tests de votre application
      - `src/resources`
        - `src/resources/application.properties`: Fichier de configuration de votre application Springboot
        - `src/resources/static` : Contient tous les fichiers statics à exposer par votre serveur web (e.g css, img, js...)
        - `src/resources/template` : Contient tous les templates html qui seront utilisés pour générer des réponses html au client (e.g usage via Thymeleaf)
      - `target`: contient les fichiers java compilés ainsi que le projet packagé (e.g <project-name>.jar)
      - `pom.xml`: le description maven de votre projet

- Votre projet est maintenant prêt pour utiliser Springboot

